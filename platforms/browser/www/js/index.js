const normalizeName = (elem, name) => {
    const makeVowel = (ch, cl) => {
        let vowel = document.createElement("span");
        vowel.className = cl;
        vowel.textContent = ch;
        return vowel;
    };
    const normalizeChar = c => {
        switch (c) {
            case "A": case "a": return makeVowel(c, "a");
            case "E": case "e": return makeVowel(c, "e");
            case "É": case "é": return makeVowel(c, "e");
            case "È": case "è": return makeVowel(c, "e");
            case "Ê": case "ê": return makeVowel(c, "e");
            case "I": case "i": return makeVowel(c, "i");
            case "O": case "o": return makeVowel(c, "o");
            case "U": case "u": return makeVowel(c, "u");
            default: return document.createTextNode(c);
        }
    };
    elem.textContent = "";
    for (const c of name) {
        elem.appendChild(normalizeChar(c));
    }
};

class PagesManager {
    constructor() {
        this.names = ["ba", "be", "bé", "bè", "bê", "bi", "bo", "bu"];
        this.index = 0;
        this.xDown = null;
        this.container = document.getElementById("card-container");
        this.play = document.getElementById("play");

        document.addEventListener('touchstart', this.startMove.bind(this), false);
        document.addEventListener('touchmove', this.move.bind(this), false);
        document.addEventListener('touchend', this.endMove.bind(this), false);

        let template = document.getElementById("card-template");
        let container = document.getElementById("card-container");
        container.style.setProperty("--n", this.names.length);

        this.names.map(name => {
            let newNode = template.cloneNode(true);
            let sound = newNode.getElementsByTagName("audio").item(0);
            let content = newNode.getElementsByClassName("front").item(0);
            let content_back = newNode.getElementsByClassName("back").item(0);

            newNode.removeAttribute("id");
            newNode.removeAttribute("style");
            sound.src = name + ".ogg";
            sound.id = name + "-sound";
            newNode.addEventListener('click', () => newNode.classList.toggle('is-flipped'));
            normalizeName(content, name.toUpperCase());
            normalizeName(content_back, name);

            return newNode;
        }).forEach(node => {
            container.appendChild(node);
        });
        this.updatePlay();
    }

    updatePlay() {
        this.play.onclick = () => document
            .getElementById(this.names[this.index] + "-sound")
            .play();
    }

    static getX(evt) {
        let touche = evt.changedTouches ? evt.changedTouches[0] : evt;

        return touche.clientX;
    }

    startMove(evt) {
        this.xDown = PagesManager.getX(evt);
        this.container.classList.toggle('smooth', false);
    }

    move(evt) {
        if (this.xDown != null) {
            var xDiff = PagesManager.getX(evt) - this.xDown;
            this.container.style.setProperty('--tx', `${Math.round(xDiff)}px`);
        }
    }

    endMove(evt) {
        if (this.xDown != null) {
            let xDiff = PagesManager.getX(evt) - this.xDown;
            let sign = Math.sign(xDiff);
            let p = (sign * xDiff / window.innerWidth).toFixed(2);

            if (p > .1 && (sign < 0 ? this.index < this.names.length - 1 : this.index > 0)) {
                this.index -= sign;
                this.container.style.setProperty("--i", this.index);
                p = 1 - p;
            }
            this.container.style.setProperty('--tx', "0px");
            this.container.style.setProperty('--f', p);

            this.container.classList.toggle('smooth', true);
            this.updatePlay();
            this.xDown = null;
        }
    }
}

var app = {
    pagesManager: new PagesManager(),

    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    },
};

app.initialize();